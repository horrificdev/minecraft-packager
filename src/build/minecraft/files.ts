import _merge from "deepmerge";

import * as _log from "../../utils/logger";
import { SourceFile } from "../base";

type MergeFn = (src: SourceFile, other: SourceFile) => SourceFile;

type FileStructure = {
	[regex: string]: FileStructure | FileNode,
};

type FileNode = {
	merge: "never" | "deep" | MergeFn,
};

const fileStructure: FileStructure = {
	"data": {
		"(?<namespace>\\w+)": {
			"dimension": {
				merge: "never"
			},
			"dimension_type": {
				merge: "never"
			},
			"functions": {
				"(?<path>[\\S]+).mcfunction": {
					merge: "never"
				}
			},
			"loot_tables": {
				"(?<path>[\\S]+).json": {
					merge: "deep"
				}
			},
			"tags\\/(blocks|items|entity_types|functions)": {
				"(?<path>[\\S]+).json": {
					merge: "deep"
				}
			}
		}
	},
	"assets": {
		"(?<namespace>\\w+)": {
			"atlases": {
				"(?<path>[\\S]+).json": {
					merge(a, b) {
						type Source = {
							type: "directory";
							source: string;
							prefix: string;
						} | {
							type: "single";
							resource: string;
							sprite?: string;
						};

						let jsonA = JSON.parse(a.text || "{}");
						let jsonB = JSON.parse(b.text || "{}");

						let sources: Source[] = jsonA.sources || [];
						jsonA.sources = sources;

						for (const source of (jsonB.sources || [])) {
							if (source.type === "directory") {
								const existing = sources.find((s) => s.type === "directory" && s.source === source.source);
								if (existing?.type === "directory" && existing.prefix !== source.prefix) {
									_log.warning(`Conflict merging ${a.name}: ${source.type} source ${source.source} has a conflicting "prefix" value.`);
								}

								if (!existing) sources.push(source);
							} else if (source.type === "single") {
								const existing = sources.find((s) => s.type === "single" && s.resource === source.resource);
								if (existing?.type === "single" && existing.sprite !== source.sprite) {
									_log.warning(`Conflict merging ${a.name}: ${source.type} source ${source.resource} has a conflicting "sprite" value.`);
								}

								if (!existing) sources.push(source);
							} else {
								sources.push(source);
							}
						}

						return {
							...a,
							text: JSON.stringify(jsonA),
						};
					}
				}
			},
			"blockstates": {
				merge: "never"
			},
			"lang": {
				merge: "deep"
			},
			"models": {
				"item": {
					"(?<path>[\\S]+).json": {
						merge(a, b) {
							let jsonA = JSON.parse(a.text || "{}");
							let jsonB = JSON.parse(b.text || "{}");

							// sort custom model overrides in order of CustomModelData
							jsonA.overrides = _merge<any[]>(jsonA.overrides || [], jsonB.overrides || [])
								.sort((a, b) => (a?.predicate?.custom_model_data||0) - (b?.predicate?.custom_model_data||0));

							return {
								...a,
								text: JSON.stringify(jsonA),
							};
						}
					}
				},
				"(block)": {
					"(?<path>[\\S]+).json": {
						merge: "never"
					}
				}
			},
			"sounds.json": {
				merge: "deep"
			}
		}
	}
};

export const fileMatches: Record<string, FileNode> = {};

function joinMatches(prefix: string, match: FileStructure|FileNode) {
	const isNode = (a: typeof match): a is FileNode => !!a.merge;
	if (isNode(match)) {
		fileMatches[prefix + "\\S*"] = match;
		return;
	}

	for (const part of Object.keys(match)) {
		joinMatches((prefix ? prefix + "\\/" : "") + part, match[part]);
	}
}

joinMatches("", fileStructure);

export type FileReference = {
	namespace: string,
	path: string,
};

function findNode(file: SourceFile) : [FileNode, Record<string, string>] | void {
	for (const key of Object.keys(fileMatches)) {
		const match = new RegExp(key, "g").exec(file.name);
		if (!match || !match.groups) continue;

		return [fileMatches[key], match.groups];
	}
}

export function reference(file: SourceFile) : FileReference|void {
	const [_, groups] = findNode(file) ?? [];

	if (groups?.namespace) return {
		namespace: groups.namespace,
		path: groups.path ?? "",
	};
}

export function merge(src: SourceFile, other: SourceFile) : SourceFile {
	const [node, _] = findNode(src) ?? [];
	if (!node) return src;

	if (typeof node.merge === "function") {
		return node.merge(src, other);
	} else if (src.name.endsWith(".json") && node.merge === "deep") {
		const srcJson = JSON.parse(src.text || "{}");
		const otherJson = JSON.parse(other.text || "{}");

		const newJson = _merge(srcJson, otherJson);

		return {
			...src,
			text: JSON.stringify(newJson),
		};
	} else {
		_log.warning(`Ignoring ${other.name} from '${other.opts.name}' as it is already provided here.`);
		return src;
	}
}
