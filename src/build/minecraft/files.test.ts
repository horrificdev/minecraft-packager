import { expect } from 'chai';
import { CompilerOptions, SourceFile } from '../base';
import * as _files from './files';

const opts = {
	type: "datapack"
} as CompilerOptions;

describe('files.ts', () => {
	describe("reference()", () => {
		it("provides function references", () => {
			const file: SourceFile = {
				opts,
				name: "data/fennifith/functions/tick.mcfunction",
				text: `say hi`
			};

			const result = _files.reference(file);
			expect(result?.namespace).to.equal("fennifith");
			expect(result?.path).to.equal("tick");
		});
	});

	describe("merge()", () => {
		it("merges tag entries", () => {
			const file1: SourceFile = {
				opts,
				name: "data/minecraft/tags/functions/tick.json",
				text: `{"values":["a:tick"]}`
			};

			const file2: SourceFile = {
				opts,
				name: "data/minecraft/tags/functions/tick.json",
				text: `{"values":["b:tick"]}`
			};

			const result = _files.merge(file1, file2);
			const resultJSON = JSON.parse(result.text!);

			expect(result.name).to.equal(file1.name);
			expect(resultJSON.values).to.include("a:tick");
			expect(resultJSON.values).to.include("b:tick");
			expect(resultJSON.values.length).to.equal(2);
		});
	});
});
