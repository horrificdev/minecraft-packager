import { compose } from "../../utils/compose";
import { withExt } from "../base";

const MAX_COMMAND_LENGTH = 255;

const minifyMcfunction = withExt(".mcfunction", function* ({opts, text, ...file}) {
	if (text && opts.minify.mcfunction != false) {
		const EMPTY_LINE = /^\s*\n/gm;
		const COMMENT = /^\s*#.*\n/gm;

		text = text.replace(EMPTY_LINE, '').replace(COMMENT, '');
	}

	yield {...file, opts, text};
});

const minifyJSON = withExt(".json", function* ({opts, text, ...file}) {
	if (text && opts.minify.json != false) {
		// minify the JSON text
		text = JSON.stringify(JSON.parse(text));
	}

	yield {...file, opts, text};
});

const lintMcfunction = withExt(".mcfunction", function* ({opts, text, ...file}) {
	if (text) {
		for (const line in text.split('\n')) {
			if (line.length > MAX_COMMAND_LENGTH) {
				const remaining = line.split('').splice(MAX_COMMAND_LENGTH).join('');
				throw `Command length exceeds ${MAX_COMMAND_LENGTH} characters: '${remaining}' will be cut off`;
			}
		}
	}

	yield {...file, opts, text};
});

export const lint = compose(
	minifyMcfunction,
	minifyJSON,
	lintMcfunction,
);
