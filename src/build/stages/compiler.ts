import _path from "path";

import _fm from "front-matter";

import * as _fsutil from '../../utils/fs';
import { SourceFile } from '../base';
import * as _filters from '../nunjucks/filters';
import _log from "../../utils/logger";
import { renderNunjucks } from "../nunjucks";
import { resolveImport } from "../../registry";
import { readFile } from "..";
import { compose } from "../../utils/compose";

async function* compileFrontmatter({text, data, ...file}: SourceFile) {
	if (text) {
		const { attributes, body } = _fm(text);

		data = Object.assign({}, data, attributes);
		text = body;
	}

	yield {...file, text, data};
};

async function* compileExtends({text, data, ...file}: SourceFile) {
	if (data.extends) {
		// resolve import path of extended file
		const [extendName, basePath] = await resolveImport(file, "_templates", data.extends);

		for await (const extendPath of _fsutil.traverse(basePath)) {
			// get relative path under base (e.g. "importdir/a/file" -> "a/file")
			const extendPathRel = _path.relative(basePath, extendPath);
			// join relative path to file location (e.g. in "usefile" -> "usefile/a/file")
			const extendPathJoin = _path.join(file.name, extendPathRel);

			const extendFile = await readFile(extendPath, file.opts, {
				...data.pkg[extendName],
				content: text,
				contentData: data,
			});

			// process frontmatter of template file
			yield* compileFrontmatter({
				...extendFile,
				name: extendPathJoin,
			});
		}

		// don't render the file itself!
		return;
	}

	yield {...file, text, data};
};

async function* compileCollections({text, data, ...file}: SourceFile) {
	if (text && _path.basename(file.name).startsWith("_")) {
		let collection = data;
		(data.collection || "").split(".").forEach((key: string) => {
			if (key.length > 0 && collection)
				collection = collection[key];
		});

		for (const [index, [key, item]] of Object.entries(collection).entries()) {
			// combine iteration variables with global data
			const itemData = Object.assign({}, data, { key, item, index });
			const itemFile = { ...file, text, data: itemData };

			// generate new filename
			const format = data.filename || `<< key >>${_path.extname(file.name)}`;
			const formatName = await renderNunjucks({...itemFile, text: format});
			itemFile.name = _path.dirname(file.name) + _path.sep + formatName;

			yield itemFile;
		}
	} else {
		yield {...file, text, data};
	}
};

async function* compileNunjucks(file: SourceFile) {
	if (file.text) {
		file.text = await renderNunjucks(file);
	}

	yield file;
}

export const compile = compose(
	compileFrontmatter,
	compileExtends,
	compileCollections,
	compileNunjucks,
);
