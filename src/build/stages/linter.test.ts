import { expect } from 'chai';
import { CompilerOptions, SourceFile } from '../base';
import { lint } from './linter';

const opts = {
	minify: {
		mcfunction: true,
		json: true,
	}
} as CompilerOptions;

describe("linter.ts", () => {
	describe("minifyJSON()", () => {
		it("should minify JSON structures", async () => {
			const test = {
				name: 'data/fennifith/functions/test.json',
				text: '  \n     {         "test":     "hello world"\n }\n\n',
				opts
			} as SourceFile;

			const expected = '{"test":"hello world"}';

			const linter = lint(test);
			const { value } = await linter.next();

			// expected: the first value matches minified output
			expect(value?.text).to.equal(expected);
			// expected: the generator does not return any more values
			expect(!!(await linter.next())?.value).to.be.false;
		});
	});

	describe("minifyMcfunction()", () => {
		it("should remove comments from .mcfunctions", async () => {
			const test = {
				name: 'data/fennifith/functions/test.mcfunction',
				text: '# hello, I am a comment\nhello world',
				opts
			} as SourceFile;

			const expected = 'hello world';

			const linter = lint(test);
			const { value } = await linter.next();

			// expected: the first value matches minified output
			expect(value?.text).to.equal(expected);
			// expected: the generator does not return any more values
			expect(!!(await linter.next())?.value).to.be.false;
		});

		it("should remove empty lines from .mcfunctions", async () => {
			const test = {
				name: 'data/fennifith/functions/test.mcfunction',
				text: '\nhello\n    \t\n world\n\n',
				opts
			} as SourceFile;

			const expected = 'hello\n world\n';

			const linter = lint(test);
			const { value } = await linter.next();

			expect(value?.text).to.equal(expected);
			// expected: the generator does not return any more values
			expect(!!(await linter.next())?.value).to.be.false;
		});
	});
});
