import _path from 'path';
import JSZip from "jszip";

import _log from "../../utils/logger";
import * as _fsutil from '../../utils/fs';
import { promises as _fs } from "../../utils/fs";
import { compose } from "../../utils/compose";
import { CompilerOptions, SourceFile } from "../base";
import { readFile, writeFile } from '..';
import { merge } from "../minecraft/files";
import { resolveFormat } from '../../utils/mcversion';

export const packager = async (opts: CompilerOptions) => {
	const zip = new JSZip();

	// clean any previous files
	try {
		const typeDir = ({
			resourcepack: "assets",
			datapack: "data"
		})[opts.type];

		const outDir = _path.join(opts.cwd, opts.outDir, typeDir);

		if ((await _fs.stat(_path.resolve(outDir)).catch(() => null))?.isDirectory())
			await _fsutil.rimraf(outDir);
	} catch (e) {
		// dir does not exist
	}

	const packMetaStr = await _fs.readFile(_path.resolve(opts.cwd, "pack.mcmeta"), { encoding: 'utf8' })
		.catch(_ => "{}") as string;

	const packMeta = JSON.parse(packMetaStr);
	packMeta.pack = {
		pack_format: resolveFormat(opts.type, opts.format),
		description: opts.description,
	};

	// add package metadata
	zip.file("pack.mcmeta", Buffer.from(JSON.stringify(packMeta), 'utf8'));

	/**
	 * Transforms the file name path into the outDir-prefixed path,
	 * and checks for duplicate files (running merge function if necessary)
	 */
	async function* packagePath({ name, ...file }: SourceFile) : AsyncGenerator<SourceFile> {
		const outFile = _path.join(opts.cwd, opts.outDir, _path.relative(opts.cwd, name));

		// if the file already exists
		if (await _fs.stat(outFile).catch(() => false)) {
			// read the existing file and merge its contents
			const existing = await readFile(outFile, opts, {});
			yield merge({ ...file, name }, existing);
		} else {
			yield { ...file, name };
		}
	}

	/**
	 * Writes the file to fs & adds to JSZip.
	 */
	async function* packageWrite({ name, ...file }: SourceFile) : AsyncGenerator<SourceFile> {
		const outFile = _path.join(opts.cwd, opts.outDir, _path.relative(opts.cwd, name));
		await writeFile({ ...file, name: outFile });

		const buffer = await _fs.readFile(outFile);

		zip.file(name, buffer);
		_log.verbose(`++ ${name}`);
	}

	return {
		packagerStage: compose(
			packagePath,
			packageWrite,
		),

		zip() {
			return zip;
		},
	};
};
