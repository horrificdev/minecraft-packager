import * as _fsutil from '../utils/fs';
import { promises as _fs } from "../utils/fs";
import _path from 'path';
import _os from "os";

import _fm from "front-matter";
import _njk from "nunjucks";
import YAML from 'yaml';

import * as _log from "../utils/logger";
import { CompilerOptions, getCompilerOptions, PackageType, SourceFile, EXT_BINARY } from "./base";
import { resolveLocal } from '../registry';
import { compose } from '../utils/compose';
import { compile } from './stages/compiler';
import { lint } from './stages/linter';
import { packager } from './stages/packager';

async function compileData(opts: CompilerOptions, noRecurse?: boolean) : Promise<any> {
	// build data tree...
	const dataDir = _path.join(opts.cwd, opts.dataDir);
	const data = {
		pkg: {
			[opts.name]: opts
		}
	};

	for await (const file of _fsutil.traverse(dataDir)) {
		// mkdir? (except it's a javascript object)
		const obj = _path.dirname(_path.relative(dataDir, file))
			.split('/')
			.filter(key => key != '.')
			.reduce<any>((curr, key) => {
				return key in curr ? curr[key] : (curr[key] = {});
			}, data);

		const key = _path.basename(file).replace(/\..*$/, '');
		if (file.endsWith(".yml")) {
			obj[key] = YAML.parse(await _fs.readFile(_path.resolve(file), { encoding: 'utf8' }) as string);
		} else {
			_log.warning(`Data file type not supported: ${file}`);
		}
	}

	_log.verbose("data =", JSON.stringify(data, null, 4));

	if (noRecurse) return data;

	// assemble package data of all dependencies
	for (const pkgName in Object.keys(opts.dependencies)) {
		const pkgDir = await resolveLocal(opts.cwd, pkgName);
		const pkgOpts = await getCompilerOptions(pkgDir, opts.type).catch(() => null);
		if (!pkgOpts) continue;

		data.pkg[pkgName] = await compileData(pkgOpts, true);
	}

	return data;
}

export async function writeFile(file: SourceFile) {
	_log.verbose(`>> ${file.name}`);
	await _fsutil.mkdirs(_path.dirname(file.name));

	const out = _path.resolve(file.name);
	if (file.text) {
		await _fs.writeFile(out, Buffer.from(file.text, 'utf8'));
	} else if (file.buffer) {
		await _fs.writeFile(out, file.buffer);
	} else {
		_log.error(file.name, "No data provided to write to this file!")
	}
}

export async function readFile(name: string, opts: CompilerOptions, data: any) : Promise<SourceFile> {
	const outName = _path.relative(opts.cwd, name);
	_log.verbose(`+ ${outName}`);
	//await _fsutil.mkdirs(_path.resolve(_path.dirname(outFile)));

	const file: SourceFile = {
		name: outName,
		opts,
		data,
	};

	// use buffer for binary files
	if (EXT_BINARY.some(ext => file.name.endsWith(ext))) {
		file.buffer = await _fs.readFile(_path.resolve(name));
	} else {
		file.text = await _fs.readFile(_path.resolve(name), { encoding: 'utf8' }) as string;
	}

	return file;
}

async function* readDependencies(arg: SourceFile) : AsyncGenerator<SourceFile> {
	const opts = await getCompilerOptions(arg.opts.cwd, arg.opts.type).catch(() => null);
	if (!opts) {
		_log.verbose("soft-fail: could not find compiler options");
		return;
	}

	const data = await compileData(opts);

	// recursively yield dependencies of the package
	for (const pkg of Object.keys(opts.dependencies)) {
		const path = await resolveLocal(opts.cwd, pkg);

		yield* readDependencies({
			opts: { cwd: path, type: opts.type } as CompilerOptions,
			name: '',
		});
	}

	// yield the root package
	yield { opts, data, name: '' };
}

async function* readPackage({ opts, data }: SourceFile) : AsyncGenerator<SourceFile> {
	for await (const file of _fsutil.traverse(opts.srcDir)) {
		yield readFile(file, opts, data);
	}
}

export async function build(cwd: string, type: PackageType, shouldWrite: boolean = true) : Promise<Uint8Array> {
	const opts = await getCompilerOptions(cwd, type);
	if (!opts) throw new Error(`Package is not configured for '${type}'.`);

	const { packagerStage, zip } = await packager(opts);

	const callback = compose(
		readDependencies,
		readPackage,
		compile,
		lint,
		packagerStage,
	);

	for await (const _ of callback({
		opts: { cwd, type } as CompilerOptions,
		name: '',
	}));

	const zipDir = _path.join(opts.cwd, opts.outDir);
	_log.verbose("JSZip: Writing zip to ", zipDir, opts.zipFile);

	// generate zip
	const zipData = await zip().generateAsync({ type: 'uint8array' });
	if (shouldWrite) await _fs.writeFile(_path.resolve(zipDir, opts.zipFile), zipData);
	return zipData;
}
