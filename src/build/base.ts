import { promises as _fs } from "../utils/fs";
import _path from "path";

import * as _mcversion from '../utils/mcversion';
import { getName } from "../registry";
import { GeneratorFn } from "../utils/compose";

// binary file extensions not to be rendered with nunjucks
export const EXT_BINARY = [".nbt", ".ogg", ".png"];

export const TYPES = ["resourcepack", "datapack"] as const;
export type PackageType = typeof TYPES[number];

export type CompilerOptions = {
	cwd: string,
	type: PackageType,
	srcDir: string,
	dataDir: string,
	outDir: string,

	name: string,
	version: string,
	format: string,
	description: string,
	author: string,
	dependencies: Record<string, string>,
	zipFile: string,

	minify: {
		mcfunction: boolean,
		json: boolean,
	}
}

export type FrontMatterOptions = {
	collection?: string,
	filename?: string,
};

export type SourceFile = {
	name: string,
	opts: CompilerOptions,
	data?: any,

	text?: string,
	buffer?: Buffer|Uint8Array,
};

/**
 * Gets the configured options for the package type.
 *
 * @returns the configured options, or "null" if they type does not match the config.
 */
export async function getCompilerOptions(cwd: string, type: PackageType) : Promise<CompilerOptions|null> {
	const pkgFile = _path.resolve(cwd, 'package.json');
	const pkgText = await _fs.readFile(pkgFile, { encoding: 'utf8' }) as string;
	const pkg = JSON.parse(pkgText);

	if (pkg.config?.type && type !== pkg.config.type)
		return null;

	const typeDir = ({
		resourcepack: "assets",
		datapack: "data"
	})[type];

	const opts: CompilerOptions = Object.assign({
		cwd, type,
		srcDir: _path.join(cwd, typeDir),
		dataDir: '_data',
		outDir: '_out',

		name: pkg.name || _path.basename(cwd),
		version: pkg.version || "0.0.0",
		description: pkg.description || "",
		author: pkg.author || "",
		format: _mcversion.resolveVersion(type, _mcversion.latestFormat[type]),
		dependencies: pkg.dependencies || {},

		minify: {},
	}, pkg.config || {});

	opts.zipFile = getName(opts);

	return opts as CompilerOptions;
}

/**
 * Creates a generator function that is only applied to the specified file extension
 */
export function withExt(ext: string, generator: GeneratorFn<SourceFile>) {
	return async function* run(arg: SourceFile) {
		if (arg.name.endsWith(ext)) {
			yield* generator(arg);
		} else {
			yield arg;
		}
	}
}
