import _path from "path";
import _njk from "nunjucks";

import { SourceFile } from "../base";
import { resolveLocal, resolveImport } from "../../registry";
import _filters from './filters';
import { promises as _fs } from "../../utils/fs";

export async function renderNunjucks(file: SourceFile) : Promise<string> {
	const loader: _njk.ILoader = {
		async: true,
		// @ts-ignore these typings don't work quite right
		async getSource(path, callback) {
			const [_, sourcePath] = await resolveImport(file, "_templates", path);
			const sourceText = await _fs.readFile(sourcePath, { encoding: 'utf8' }) as string;

			callback(null, {
				src: sourceText,
				path: sourcePath,
				noCache: false
			});
		}
	};

	const env = new _njk.Environment(
		loader,
		{
			autoescape: false,
			tags: {
				blockStart: '<%',
				blockEnd: '%>',
				variableStart: '<<',
				variableEnd: '>>',
				commentStart: '<#',
				commentEnd: '#>'
			}
		}
	);

	// add defined nunjucks filters/utils
	const filters = _filters(file);
	for (const [name, func] of Object.entries(filters)) {
		env.addFilter(name, func);
	}

	return new Promise((resolve, reject) => {
		if (!file.text) {
			reject("renderNunjucks: File text not provided");
			return;
		}

		env.renderString(file.text, file.data, (err, res) => {
			if (res) resolve(res);
			else reject(err);
		});
	});
}
