import _path from "path";
import { SourceFile } from "../base";
import { reference } from "../minecraft/files";

export default (file: SourceFile) => ({
	// << "../other" | namespace >> returns a namespaced ID of the file
	namespace(path: string) {
		path = _path.join(file.name, path);
		return reference({
			...file,
			name: path
		});
	},

	// << data | dump_nbt >> dumps the JSON info in minecraft's nbt syntax
	dump_nbt(data: any) {
		return JSON.stringify(data)
			// strip quotes from property names
			.replace(/(?<![\\:])(\"(?:[^\r\n:^"]|\\")+\")\s*:/gm, (match) => match.slice(1, -2) + ':')
			// fix integer array syntax: ["I; 0", 0] -> [I; 0, 0]
			.replace(/\[\"(I; *-?\d+)\"/g, "[$1")
			// remove line breaks
			.replace(/\r?\n|\r/g, '');
	}
});
