import { isBrowser } from 'browser-or-node';
import _path from 'path';

import _diskfs from 'fs';
import LightningFS from '@isomorphic-git/lightning-fs';

export const fs = isBrowser ? new LightningFS('mc-packager') : _diskfs;
export const promises = fs.promises;
export default fs;

export async function rimraf(dir: string) : Promise<void> {
	const stat = await promises.stat(dir);
	if (stat.isFile() || stat.isSymbolicLink()) {
		await promises.unlink(dir);
		return;
	}

	const files = await promises.readdir(dir);
	for (const file of files) {
		const filePath = _path.join(dir, file.toString());
		await rimraf(filePath);
	};
}

export async function mkdirs(dir: string) : Promise<void> {
	const parts = _path.resolve(dir).split(_path.sep);

	let path = '';
	for (const part of parts) {
		if (part.length == 0)
			continue;

		path += _path.sep + part;
		try { // check if dir exists
			await promises.stat(path);
		} catch (e) { // create dir
			await promises.mkdir(path)
				.catch(e => null); // dir might already exist
		}
	}
}

export async function* traverse(path: string) : AsyncGenerator<string> {
	const stat = await promises.stat(_path.resolve(path)).catch(() => null);
	if (stat === null) return;

	if (stat.isDirectory()) {
		// is a directory: recurse downwards
		const dir = await promises.readdir(_path.resolve(path)).catch(() => null);
		if (!dir) return [];

		for await (const entry of dir) {
			const entryPath = _path.join(path, entry.toString());
			yield* traverse(entryPath);
		}
	} else if (stat.isFile()) {
		// is a file: return self
		yield path;
	}
}
