export type GeneratorFn<T> = ((arg: T) => AsyncGenerator<T, void>) | ((arg: T) => Generator<T, void>);

async function* runCompose<T>(callbacks: GeneratorFn<T>[], arg: T) : AsyncGenerator<T> {
	const [current, ...rest] = callbacks;

	for await (const nextArg of current(arg)) {
		try {
			if (rest.length)
				yield* runCompose(rest, nextArg);
			else yield nextArg;
		} catch (e) {
			console.error(e, nextArg);
		}
	}
}

export function compose<T>(...callbacks: GeneratorFn<T>[]) {
	return (arg: T) => runCompose(callbacks, arg);
}
