import { createInterface } from 'readline';

export function prompt(str: string, current: string|null = null) : Promise<string> {
	const i = createInterface({
		input: process.stdin,
		output: process.stdout,
		terminal: false
	});

	return new Promise((resolve, reject) => {
		i.question(str + (current ? ` (${current})` : '') + ': ', answer => {
			answer = answer.trim();
			resolve(answer || (current || ""));
			i.close();
		});
	});
}
