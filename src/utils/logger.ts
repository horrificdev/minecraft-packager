import _chalk from "chalk";

function print(format: _chalk.Chalk, msg: any[]) {
	console.log(format(...msg));
}

export function log(...msg: any[]) {
	print(_chalk, msg);
}

export function info(...msg: any[]) {
	print(_chalk.blueBright, msg);
}

export function verbose(...msg: any[]) {
	if (process.argv.includes("-v") || process.argv.includes("--verbose"))
		print(_chalk.gray, msg);
}

export function warning(...msg: any[]) {
	print(_chalk.yellowBright, msg);
}

export function error(...msg: any[]) {
	print(_chalk.bgRed.white, msg);
}

export default { log, info, verbose, warning, error };
