import { PackageType } from "../build/base";

export const _versions = {
	"resourcepack": {
		'1.[6-8].*': 1,
		"1.(9|10).*": 2,
		"1.(11|12).*": 3,
		"1.(13|14).*": 4,
		"1.(15.*|16.[0-1])": 5,
		"1.16.[2-5]": 6,
		"1.17.*": 7,
		"1.18.*": 8,
		"1.19.[0-2]": 9,
		"1.19.3": 12,
		"1.19.4": 13,
		"1.20.[0-1]": 15,
	},
	"datapack": {
		'1.[6-8].*': 1,
		"1.(9|10).*": 2,
		"1.(11|12).*": 3,
		"1.(13|14).*": 4,
		"1.(15.*|16.[0-1])": 5,
		"1.16.[2-5]": 6,
		"1.17.*": 7,
		"1.18.[0-1]": 8,
		"1.18.2": 9,
		"1.19.[0-3]": 10,
		"1.19.4": 12,
		"1.20.[0-1]": 15,
	}
};

export const latestFormat = {
	"resourcepack": Object.values(_versions["resourcepack"]).slice(-1)[0],
	"datapack": Object.values(_versions["datapack"]).slice(-1)[0],
};

export function resolveFormat(type: PackageType, version: string|number) : number {
	if (version.toString().split(".").length == 2)
		version += ".0"; // "1.17" -> "1.17.0"

	for (const [matchStr, format] of Object.entries(_versions[type])) {
		const regexStr = matchStr.trim()
			.replace('.', '\\.')
			.replace('*', '\\d+');

		if (new RegExp(regexStr).test(version.toString()))
			return format;
	}

	// if none found, return latest version
	return latestFormat[type];
}

export function resolveVersion(type: PackageType, format: number) : string {
	return Object.entries(_versions[type])
		.find(([_, fmt]) => format === fmt)![0]
		.replace(/\[(\d)\-\d\]/, '$1')
		.replace(/\(([\w\.\*]+)\|[\w\.\*]+\)/, '$1')
		.replace(/\.\*$/, '')
		.replace('*', '0');
}
