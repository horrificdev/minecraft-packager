import { expect } from 'chai';
import { CompilerOptions, SourceFile } from '../build/base';
import * as registry from './index';


describe("registry/index.ts", () => {
	describe("resolveImport()", () => {
		const file: SourceFile = {
			name: 'data/fennifith/functions/tick.mcfunction',
			opts: {
				type: "datapack",
				name: "example",
				cwd: "/home/james/minecraft"
			} as CompilerOptions
		};

		it("returns a relative path from the file", async () => {
			const [pkg, path] = await registry.resolveImport(file, "data", "./load.mcfunction");

			expect(pkg).to.equal("example");
			expect(path).to.equal(`${file.opts.cwd}/data/fennifith/functions/load.mcfunction`);
		});

		it("returns an absolute path from the project root", async () => {
			const [pkg, path] = await registry.resolveImport(file, "data", "@/fennifith/functions/load.mcfunction");

			expect(pkg).to.equal("example");
			expect(path).to.equal(`${file.opts.cwd}/data/fennifith/functions/load.mcfunction`);
		});

		it("returns an absolute path from the same project id", async () => {
			const [pkg, path] = await registry.resolveImport(file, "data", "example//fennifith/functions/load.mcfunction");

			expect(pkg).to.equal("example");
			expect(path).to.equal(`${file.opts.cwd}/data/fennifith/functions/load.mcfunction`);
		});
	});
});