import { promises as _fs } from "../utils/fs";
import _path from "path";
import _os from "os";

import { PackageType, getCompilerOptions, SourceFile } from "../build/base";
import _log from '../utils/logger';

import _pkg from "../../package.json";

export async function getPath(pkg: { name: string, version: string, type: PackageType }) : Promise<string> {
	// TODO: make platform-agnostic?
	const pkgDir = _path.join(_os.homedir(), ".local", "share", _pkg.name, "registry");
	await _fs.mkdir(pkgDir, { recursive: true });
	return _path.join(pkgDir, getName(pkg));
}

export function getName(pkg: { name: string, version: string, type: PackageType }) : string {
	return `${pkg.name}-${pkg.version}.${pkg.type}.zip`;
}

export async function resolveLocal(cwd: string, pkgName: string) : Promise<string> {
	// TODO: automatically fetch from NPM?
	// TODO: if no node_modules dir, traverse upwards
	return _path.join(cwd, "node_modules", pkgName);
}

export async function resolveLocalZip(cwd: string, pkgName: string, type: PackageType) : Promise<string | null> {
	const pkgDir = await resolveLocal(cwd, pkgName);
	const opts = await getCompilerOptions(pkgDir, type);
	if (!opts) return null; // soft-fail: config for type not defined
	return _path.join(
		await resolveLocal(cwd, pkgName),
		"_out",
		opts.zipFile
	);
}

export async function resolveImport(file: SourceFile, base: string, path: string) : Promise<[string, string]> {
	if (path.startsWith(".")) {
		// reference relative to the current file
		return [
			file.opts.name,
			_path.join(file.opts.cwd, _path.dirname(file.name), path)
		];
	} else if (path.startsWith("@/")) {
		// reference relative to the current package
		return [
			file.opts.name,
			_path.join(file.opts.cwd, base, path.substring(2))
		];
	} else {
		// reference another package
		const pkg = path.split("//")[0];
		const dir = path.substring(pkg.length + 2);

		// if this is the referenced package, use relative return case
		if (pkg === file.opts.name)
			return resolveImport(file, base, "@/" + dir);

		return [
			pkg,
			_path.join(await resolveLocal(file.opts.cwd, pkg), dir)
		];
	}
}

export async function publishLocal(cwd: string, type: PackageType) {
	const opts = await getCompilerOptions(cwd, type);
	if (!opts) return; // soft-fail: config for type not defined

	const path = await getPath(opts);

	const buffer = await _fs.readFile(opts.zipFile);
	await _fs.writeFile(path, buffer);
	_log.info(`Published "local:${opts.name}" ${opts.version}`);
}
