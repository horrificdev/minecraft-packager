import { publishLocal } from "../registry";

async function run(args: any) {
	const doDatapack = !!args["--datapack"];
	const doResourcepack = !!args["--resourcepack"];

	const srcDir = process.cwd();

	if (doDatapack || !doResourcepack) {
		await publishLocal(srcDir, "datapack");
	}

	if (doResourcepack || !doDatapack) {
		await publishLocal(srcDir, "resourcepack");
	}
}

export default {
	names: ["publish"],
	help: ``,
	args: {
		'--datapack': Boolean,
		'--resourcepack': Boolean
	},
	run
}
