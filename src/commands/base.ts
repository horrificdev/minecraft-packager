type CommandFunction = (args: any) => Promise<any>

type Command = {
	names: string[],
	help: string,
	args: any,
	run: CommandFunction
}
