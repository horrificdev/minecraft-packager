import { TYPES, getCompilerOptions } from "../build/base";
import { build } from "../build";

async function run(args: any) {
	const types = TYPES.filter(type => (args["--type"] || type) == type);
	const srcDir = process.cwd();

	for (const type of types) {
		const opts = await getCompilerOptions(srcDir, type);
		if (!opts) continue; // soft-fail: config for type not defined

		await build(srcDir, type);
	}
}

export default {
	names: ["build"],
	help: ``,
	args: {
		'--type': String
	},
	run
}
