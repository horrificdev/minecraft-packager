.PHONY: install nvm build clean all

ifeq ($(shell test -f "$(HOME)/.nvm/nvm.sh"; echo $$?),0)
NVM := . $(HOME)/.nvm/nvm.sh && nvm use &> /dev/null
else
NVM := true
endif

ifeq (,$(shell $(NVM) && which pnpm))
NPM := $(NVM) && npm
else
NPM := $(NVM) && pnpm
endif

all: build
	$(NPM) link -g

install: package-install.lock

package-install.lock: package.json
	$(NPM) install
	touch package-install.lock

build: install
	$(NPM) build

test: install
	$(NPM) test

clean:
	rm -f package-install.lock
	rm -f package-lock.json
	rm -f pnpm-lock.yaml
	rm -rf dist/
	rm -rf node_modules/
