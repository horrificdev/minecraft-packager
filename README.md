Minecraft Packager
[![NPM Package](https://img.shields.io/npm/v/minecraft-packager?color=red&logo=npm)](https://www.npmjs.com/package/minecraft-packager)
=====

This is a multipurpose templating & linting tool for Minecraft datapacks & resource packs. It can import and merge other pack data and assets, run on command-line or in the browser, and centralize data or configuration options to template into different parts of your project.

## Command-line use

> The `--type (datapack|resourcepack)` flag can be appended to any command to declare a specific package type. By default, the packager will execute both.

`minecraft-packager init` will initialize the current directory as a datapack/resourcepack.

`minecraft-packager build` will compile the files in the `/datapack` and `/resourcepack` directories. Compiled files will be generated in the `/_out` directory.

`minecraft-packager install` will install the compiled resourcepack/datapack to the specified Minecraft location with the `--mcdir` flag, the `$MINECRAFT_HOME` environment variable, or `~/.minecraft`, in that order.

## Configuration

After initialization, the packager will create a default `package.json` file with some properties that specify the package information:

```json
{
  "config": {
    "format": "1.17",
    "dependencies": [
      "file:./included-resource-pack.zip"
    ],
    "zipFiles": {
      "resourcepack": "custom-zip-name.resourcepack.zip",
      "datapack": "custom-zip-name.datapack.zip"
    }
  }
}
```

This file is also compatible with `npm`, so the package can be compiled & installed with just `npm run dev`.

## Templating Syntax

`.mcfunction` and `.json` files can make use of a modified [Nunjucks](https://mozilla.github.io/nunjucks/templating.html) templating language to generate code:
- Custom syntax uses `<< variable >>` and `<% tag %>` instead of the curly braces.
- Any `.yml` or `.json` files nested in the `/_data` directory are available as a global variable.
  * For example, the `info` property in `/data/blocks/sponge.yml` can be referenced as `blocks.sponge.info`.
- Special filters can be used for interacting with JSON or NBT data:
  * `<< variable | dump >>` can be used to output any variable as JSON text
  * `<< info.nbt_requirements | dump_nbt >>` will form an [NBT string](https://minecraft.fandom.com/wiki/NBT_format) from the `nbt_requirements` property in `/_data/info.yml`

### Generated Files

A set of files can be "generated" from an array or object in the `/_data` directory by prefixing its filename with an underscore and adding some front matter to specify the data to use:

```text
---
collection: config.teams
filename: '<< key | lower >>.json'
---
{
	"name": "<< item.name >>",
}
```

`collection` specifies the object or array to iterate through, and `filename` generates the file name for each item in the collection.

For each generated item, the following variables are made available for templating (in addition to the global data):
- `key`: The item key (for objects, this is the property name; for arrays, the item index)
- `item`: The value of the current item
- `index`: The current iteration number

## Linting

- TODO: parse/validate certain command structures, e.g. `execute if entity ... run ...`
- TODO: parse/validate declared scoreboard names & uses
- TODO: correctly merge loot_tables & other specific JSON files with specific implementation
- TODO: ability to import files from MC source (implement as node module?)
- TODO: refactor "datapack" / "resourcepack" dirs to "data" and "assets"
